var chat = {
	message:[],
	type_message:function(){
		let message = document.getElementById('message-input');
		this.save_message(message);
		this.createPlaceholder();
	},
	hide_placeholder(){
		document.getElementById('placeholder').style.display = "none"
		this.foucs_on_message_input();
	},
	foucs_on_message_input(){
		document.getElementById('message-input').innerHTML = "&nbsp";
	},
	save_message(message){
		var date = new Date();
		let li = document.createElement('LI');
		li.innerHTML = message.textContent + "<small class='small'> "+ date + "</small>"; 
		let br = document.createElement('BR');
		let chat_ul  = document.getElementById('chat-ul');
		chat_ul.appendChild(li);
		chat_ul.appendChild(br);
		chat_ul.scrollTop = chat_ul.scrollHeight;
	},
	createPlaceholder(){
       let messageInput  = document.getElementById('message-input');
       messageInput.innerHTML = '<span id="placeholder">Type Message Here........................................................</span>';
	}
}